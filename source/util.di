// D import file generated from '../SHS/source/util.d'
module util;
import terminal : Terminal;
import std.typecons : Tuple;
alias ExecFunctions = Tuple!(string, bool delegate(string[] args, Terminal terminal), string)[];
template DefaultTerminalMode(T, string mode, string prefix)
{
	import util : StaticInstance;
	import util : DefaultHasCommand;
	import util : DefaultOnCommand;
	import util : DefaultOnOpen;
	import util : DefaultOnClose;
	import util : ExecFunctions;
	import util : DefaultGetExecFunctions;
	import util : syntaxError;
	private ExecFunctions execFunctions;
	mixin StaticInstance!T;
	mixin DefaultHasCommand!();
	mixin DefaultOnCommand!mode;
	mixin DefaultOnOpen!(prefix, mode);
	mixin DefaultOnClose!mode;
	mixin DefaultGetExecFunctions!();
}
template StaticInstance(T)
{
	static T instance()
	{
		import std.concurrency : initOnce;
		__gshared T objInstance;
		return initOnce!objInstance(new T);
	}
}
template DefaultHasCommand()
{
	bool hasCommand(string command, Terminal terminal)
	{
		foreach (execFun; this.execFunctions)
		{
			if (execFun[0] == command)
			{
				return true;
			}
		}
		return false;
	}
}
template DefaultOnCommand(string mode)
{
	bool onCommand(string command, string[] args, Terminal terminal)
	{
		import std.format : format;
		import util : execOnCommand;
		if (!hasCommand(command, terminal))
		{
			terminal.writeln(format!"SHSCli is in %s!"(mode));
			terminal.writeln(format!"The \"%s\" command is not supported by this mode"(command));
			return false;
		}
		return execOnCommand(command, args, terminal, this.execFunctions);
	}
}
template DefaultOnOpen(string prefix, string mode)
{
	void onOpen(Terminal terminal)
	{
		import std.stdio : writeln;
		import std.format : format;
		terminal.setCommandModePrefix(prefix);
		writeln("SHSCli - Ship Hydrostatics and Stability Command Line interface");
		writeln(format!"Welcome in SHSCLI %s!"(mode));
		writeln("Press \"HELP\" to get more information");
		writeln();
		writeln();
	}
}
template DefaultOnClose(string mode)
{
	void onClose(Terminal terminal)
	{
		import std.stdio : writeln;
		import std.format : format;
		writeln();
		writeln();
		writeln(format!"Exiting %s in progress..."(mode));
	}
}
template DefaultGetExecFunctions()
{
	ExecFunctions getExecFunctions(Terminal terminal)
	{
		return this.execFunctions;
	}
}
bool execOnCommand(string command, string[] args, Terminal terminal, ExecFunctions execFunctions);
bool syntaxError(string command, Terminal terminal);
