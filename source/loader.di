// D import file generated from '../SHS/source/loader.d'
module loader;
import terminal : Terminal, TerminalMode;
class LoaderMode : TerminalMode
{
	import util : DefaultTerminalMode;
	mixin DefaultTerminalMode!(LoaderMode, "LOADER mode", "LOADER> ");
	private static void*[string] dllPtrs;
	static ~this();
	this()
	{
		import std.typecons : tuple;
		this.execFunctions ~= tuple("DLOPEN", &this.loadMode, "Load mode in SHS [path of mode]");
		this.execFunctions ~= tuple("DLCLOSE", &this.unloadMode, "Remove mode from SHS [name of mode]");
		this.execFunctions ~= tuple("MODES", &this.showModes, "View the bootable modes");
		this.execFunctions ~= tuple("RUN", &this.setMode, "Start the mode [name of mode]");
		this.execFunctions ~= tuple("HELP", &this.help, "Display the help message");
		this.execFunctions ~= tuple("EXIT", &this.exit, "Exit from the current mode");
		this.execFunctions ~= tuple("RESET", &this.resetTerminalMode, "Reset the current mode");
	}
	void reset(Terminal terminal);
	private bool loadMode(string[] args, Terminal terminal);
	private bool unloadMode(string[] args, Terminal terminal);
	private bool showModes(string[] args, Terminal terminal);
	private bool setMode(string[] args, Terminal terminal);
	private bool help(string[] args, Terminal terminal);
	private bool exit(string[] args, Terminal terminal);
	private bool resetTerminalMode(string[] args, Terminal terminal);
}
