// D import file generated from '../SHS/source/terminal.d'
module terminal;
interface TerminalMode
{
	import util : ExecFunctions;
	bool hasCommand(string command, Terminal terminal);
	bool onCommand(string command, string[] args, Terminal terminal);
	void onOpen(Terminal terminal);
	void onClose(Terminal terminal);
	ExecFunctions getExecFunctions(Terminal terminal);
	void reset(Terminal terminal);
}
class Terminal
{
	import std.stdio : File;
	import std.typecons : Tuple;
	private File inFile;
	private File outFile;
	private TerminalMode currentMode;
	private TerminalMode rootMode;
	private string cmdModePrefix;
	private TerminalMode[string] modes;
	public this(File inFile, File outFile)
	{
		this.inFile = inFile;
		this.outFile = outFile;
	}
	public void init();
	public void setTerminalMode(TerminalMode mode);
	public void setRootMode(TerminalMode mode);
	public TerminalMode getTerminalMode();
	public TerminalMode getRootMode();
	public void addTerminalMode(TerminalMode mode, string name);
	public void removeTerminalMode(string name);
	public TerminalMode[string] getTerminalModes();
	public void setCommandModePrefix(string cmdModePrefix);
	public void write(S...)(S args)
	{
		import std.stdio : File;
		this.outFile.write(args);
	}
	void writeln(S...)(S args)
	{
		import std.stdio : File;
		this.outFile.writeln(args);
	}
	private Tuple!(string, string[]) parseCommand(string lineInput);
}
