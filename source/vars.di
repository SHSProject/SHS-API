// D import file generated from '../SHS/source/vars.d'
module vars;
class Vars
{
	private double[string] vars;
	private static double[string] globalVars;
	public static void setGlobal(string varName, double value);
	public static double getGlobal(string varName);
	public static void resetGlobal();
	public static string[] getGlobalNames();
	public static bool checkGlobalName(string varName);
	public this(string[] varsNames...)
	{
		foreach (varName; varsNames)
		{
			this.vars[varName] = (double).init;
		}
	}
	public void set(string varName, double value);
	public double get(string varName);
	public void reset();
	public string[] getNames();
	public bool checkName(string varName);
}
